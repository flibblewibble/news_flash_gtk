mod article_row;
mod models;
mod single;

use crate::app::Action;
use crate::content_page::ContentHeader;
use crate::content_page::HeaderSelection;
use crate::i18n::{i18n, i18n_f};
use crate::main_window_state::MainWindowState;
use crate::settings::Settings;
use crate::sidebar::models::SidebarSelection;
use crate::util::{BuilderHelper, GtkUtil, Util};
use gdk::RGBA;
use glib::{clone, translate::ToGlib, Sender};
use gtk::{Label, LabelExt, ListBoxExt, ListBoxRowExt, ScrolledWindow, Stack, StackExt, StackTransitionType};
use log::warn;
use models::ArticleListChangeSet;
pub use models::{ArticleListArticleModel, ArticleListModel, MarkUpdate, ReadUpdate};
use news_flash::models::{ArticleID, Marked, Read};
use parking_lot::RwLock;
use single::SingleArticleList;
use std::sync::Arc;

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum CurrentList {
    List1,
    List2,
    Empty,
}

pub struct ArticleList {
    sender: Sender<Action>,
    stack: Stack,
    list_1: SingleArticleList,
    list_2: SingleArticleList,
    list_model: ArticleListModel,
    list_activate_signal: Option<usize>,
    local_state: MainWindowState,
    global_state: Arc<RwLock<MainWindowState>>,
    current_list: CurrentList,
    settings: Arc<RwLock<Settings>>,
    empty_label: Label,
}

impl ArticleList {
    pub fn new(
        settings: &Arc<RwLock<Settings>>,
        content_header: &Arc<ContentHeader>,
        global_state: &Arc<RwLock<MainWindowState>>,
        sender: Sender<Action>,
    ) -> Self {
        let builder = BuilderHelper::new("article_list");
        let stack = builder.get::<Stack>("article_list_stack");
        let empty_scroll = builder.get::<ScrolledWindow>("empty_scroll");
        let empty_label = builder.get::<Label>("empty_label");

        let list_1 = SingleArticleList::new(sender.clone(), content_header.clone());
        let list_2 = SingleArticleList::new(sender.clone(), content_header.clone());

        let local_state = MainWindowState::new();
        let model = ArticleListModel::new(&settings.read().get_article_list_order());

        stack.add_named(&list_1.widget(), "list_1");
        stack.add_named(&list_2.widget(), "list_2");
        stack.add_named(&empty_scroll, "empty");

        let settings = settings.clone();

        let mut article_list = ArticleList {
            sender,
            stack,
            list_1,
            list_2,
            list_model: model,
            list_activate_signal: None,
            local_state,
            global_state: global_state.clone(),
            current_list: CurrentList::List1,
            settings,
            empty_label,
        };

        article_list.setup_list_selected_singal();

        article_list
    }

    pub fn widget(&self) -> gtk::Stack {
        self.stack.clone()
    }

    pub fn get_background_color(&self) -> RGBA {
        self.list_1.get_background_color()
    }

    pub fn get_relevant_article_count(&self, header_selection: &HeaderSelection) -> usize {
        self.list_model.get_relevant_count(header_selection)
    }

    pub fn update(&mut self, mut new_list: ArticleListModel, new_state: &Arc<RwLock<MainWindowState>>) {
        let requre_new_list = self.require_new_list(&new_state);

        self.stack
            .set_transition_type(self.calc_transition_type(requre_new_list, new_state));

        // check if list model is empty and display a message
        if new_list.len() == 0 {
            self.empty_label.set_label(&self.compose_empty_message(new_state));
            self.stack.set_visible_child_name("empty");

            // clear old list
            if let Some(current_list) = self.get_current_list() {
                current_list.clear(None);
                GtkUtil::disconnect_signal(self.list_activate_signal, &current_list.list());
            }

            self.list_activate_signal.take();
            self.current_list = CurrentList::Empty;
        } else {
            // check if a new list is reqired or current list should be updated
            if requre_new_list {
                // compare with empty model
                let empty_list = ArticleListModel::new(&self.settings.read().get_article_list_order());
                let diff = empty_list.generate_diff(&new_list);
                self.switch_lists();
                self.execute_diff(diff);
            } else {
                // don't remove selected article during update
                // the `require_new_list` check above already ensures the old and new state are the same
                // so we add the selected model to the new_list so it wont be removed during the update
                if let Some(current_list) = self.get_current_list() {
                    let selected_index = current_list.get_selected_index();
                    if let Some(selected_index) = selected_index {
                        if let Some(selected_article_model) = self.list_model.calculate_selection(selected_index) {
                            if !new_list.contains(&selected_article_model.id) {
                                new_list.add_model(vec![selected_article_model.clone()]);
                            } else {
                                warn!("New list already contains selected article");
                            }
                        }
                    }
                }

                let diff = self.list_model.generate_diff(&new_list);
                self.execute_diff(diff);
            };
        }

        self.list_model = new_list;
        self.local_state = new_state.read().clone();
    }

    pub fn add_more_articles(&mut self, new_list: ArticleListModel) {
        let list = match self.current_list {
            CurrentList::List1 => &self.list_1,
            CurrentList::List2 => &self.list_2,
            CurrentList::Empty => return,
        };

        for model in new_list.models() {
            if !self.list_model.contains(&model.id) {
                list.add_idle(&model, -1, &self.global_state);
            } else {
                warn!("trying to add article which already exists: {}", model.id);
            }
        }

        self.list_model.add_model(new_list.models().clone());
    }

    fn execute_diff(&self, diff: Vec<ArticleListChangeSet>) {
        let list = match self.current_list {
            CurrentList::List1 | CurrentList::Empty => &self.list_1,
            CurrentList::List2 => &self.list_2,
        };

        for diff in diff {
            match diff {
                ArticleListChangeSet::Add(article, pos) => {
                    list.add(article, pos, &self.global_state);
                }
                ArticleListChangeSet::Remove(id) => {
                    list.remove(id.clone());
                }
                ArticleListChangeSet::UpdateMarked(id, marked) => {
                    list.set_article_row_state(&id, None, Some(marked));
                }
                ArticleListChangeSet::UpdateRead(id, read) => {
                    list.set_article_row_state(&id, Some(read), None);
                }
                ArticleListChangeSet::UpdateDateString(id, date) => {
                    list.update_date_string(&id, date);
                }
            }
        }
    }

    fn switch_lists(&mut self) {
        self.current_list = match self.current_list {
            CurrentList::List1 => {
                self.stack.set_visible_child_name("list_2");
                CurrentList::List2
            }
            CurrentList::List2 | CurrentList::Empty => {
                self.stack.set_visible_child_name("list_1");
                CurrentList::List1
            }
        };

        self.setup_list_selected_singal();

        // clear old list after animation is done
        match self.current_list {
            CurrentList::List1 => self.list_2.clear(Some(110)),
            CurrentList::List2 => self.list_1.clear(Some(110)),
            CurrentList::Empty => {}
        };
    }

    fn setup_list_selected_singal(&mut self) {
        let (new_list, old_list) = match self.current_list {
            CurrentList::List1 => (&self.list_1, &self.list_2),
            CurrentList::List2 => (&self.list_2, &self.list_1),
            CurrentList::Empty => return,
        };
        GtkUtil::disconnect_signal(self.list_activate_signal, &old_list.list());
        let activate_signal_id = new_list
            .list()
            .connect_row_activated(clone!(
                @strong self.sender as sender => @default-panic, move |_list, row|
            {
                let selected_index = row.get_index();
                Util::send(&sender, Action::ArticleListIndexSelected(selected_index));
            }))
            .to_glib();
        self.list_activate_signal.replace(activate_signal_id as usize);
    }

    fn require_new_list(&self, new_state: &RwLock<MainWindowState>) -> bool {
        if self.local_state == *new_state.read()
            && self.settings.read().get_article_list_order() == self.list_model.order()
            && self.current_list != CurrentList::Empty
        {
            return false;
        }
        true
    }

    fn calc_transition_type(
        &self,
        require_new_list: bool,
        new_state: &Arc<RwLock<MainWindowState>>,
    ) -> StackTransitionType {
        if require_new_list {
            match self.local_state.get_header_selection() {
                HeaderSelection::All => match new_state.read().get_header_selection() {
                    HeaderSelection::All => {}
                    HeaderSelection::Unread | HeaderSelection::Marked => return StackTransitionType::SlideLeft,
                },
                HeaderSelection::Unread => match new_state.read().get_header_selection() {
                    HeaderSelection::All => return StackTransitionType::SlideRight,
                    HeaderSelection::Unread => {}
                    HeaderSelection::Marked => return StackTransitionType::SlideLeft,
                },
                HeaderSelection::Marked => match new_state.read().get_header_selection() {
                    HeaderSelection::All | HeaderSelection::Unread => return StackTransitionType::SlideRight,
                    HeaderSelection::Marked => {}
                },
            }
        }
        StackTransitionType::Crossfade
    }

    fn compose_empty_message(&self, new_state: &RwLock<MainWindowState>) -> String {
        match new_state.read().get_sidebar_selection() {
            SidebarSelection::All => match new_state.read().get_header_selection() {
                HeaderSelection::All => match new_state.read().get_search_term() {
                    Some(search) => i18n_f("No articles that fit \"{}\".", &[&search]),
                    None => i18n("No articles."),
                },
                HeaderSelection::Unread => match new_state.read().get_search_term() {
                    Some(search) => i18n_f("No unread articles that fit \"{}\".", &[&search]),
                    None => i18n("No unread articles."),
                },
                HeaderSelection::Marked => match new_state.read().get_search_term() {
                    Some(search) => i18n_f("No starred articles that fit \"{}\".", &[&search]),
                    None => i18n("No starred articles."),
                },
            },
            SidebarSelection::Category(_id, title) => match new_state.read().get_header_selection() {
                HeaderSelection::All => match new_state.read().get_search_term() {
                    Some(search) => i18n_f("No articles that fit \"{}\" in category \"{}\".", &[&search, &title]),
                    None => i18n_f("No articles in category \"{}\".", &[&title]),
                },
                HeaderSelection::Unread => match new_state.read().get_search_term() {
                    Some(search) => i18n_f(
                        "No unread articles that fit \"{}\" in category \"{}\".",
                        &[&search, &title],
                    ),
                    None => i18n_f("No unread articles in category \"{}\".", &[&title]),
                },
                HeaderSelection::Marked => match new_state.read().get_search_term() {
                    Some(search) => i18n_f(
                        "No starred articles that fit \"{}\" in category \"{}\".",
                        &[&search, &title],
                    ),
                    None => i18n_f("No starred articles in category \"{}\".", &[&title]),
                },
            },
            SidebarSelection::Feed(_id, _parent_id, title) => match new_state.read().get_header_selection() {
                HeaderSelection::All => match new_state.read().get_search_term() {
                    Some(search) => i18n_f("No articles that fit \"{}\" in feed \"{}\".", &[&search, &title]),
                    None => i18n_f("No articles in feed \"{}\".", &[&title]),
                },
                HeaderSelection::Unread => match new_state.read().get_search_term() {
                    Some(search) => i18n_f("No unread articles that fit \"{}\" in feed \"{}\".", &[&search, &title]),
                    None => i18n_f("No unread articles in feed \"{}\".", &[&title]),
                },
                HeaderSelection::Marked => match new_state.read().get_search_term() {
                    Some(search) => i18n_f(
                        "No starred articles that fit \"{}\" in feed \"{}\".",
                        &[&search, &title],
                    ),
                    None => i18n_f("No starred articles in feed \"{}\".", &[&title]),
                },
            },
            SidebarSelection::Tag(_id, title) => match new_state.read().get_header_selection() {
                HeaderSelection::All => match new_state.read().get_search_term() {
                    Some(search) => i18n_f("No articles that fit \"{}\" in tag \"{}\".", &[&search, &title]),
                    None => i18n_f("No articles in tag \"{}\".", &[&title]),
                },
                HeaderSelection::Unread => match new_state.read().get_search_term() {
                    Some(search) => i18n_f("No unread articles that fit \"{}\" in tag \"{}\".", &[&search, &title]),
                    None => i18n_f("No unread articles in tag \"{}\".", &[&title]),
                },
                HeaderSelection::Marked => match new_state.read().get_search_term() {
                    Some(search) => i18n_f("No starred articles that fit \"{}\" in tag \"{}\".", &[&search, &title]),
                    None => i18n_f("No starred articles in tag \"{}\".", &[&title]),
                },
            },
        }
    }

    fn get_current_list(&self) -> Option<&SingleArticleList> {
        match self.current_list {
            CurrentList::List1 => Some(&self.list_1),
            CurrentList::List2 => Some(&self.list_2),
            CurrentList::Empty => None,
        }
    }

    pub fn select_next_article(&self) {
        self.select_article(1)
    }

    pub fn select_prev_article(&self) {
        self.select_article(-1)
    }

    fn select_article(&self, direction: i32) {
        if let Some(current_list) = self.get_current_list() {
            let selected_index = current_list.get_selected_index();
            if let Some(selected_index) = selected_index {
                let selected_row = self.list_model.calculate_selection(selected_index).cloned();
                let next_row = self.list_model.calculate_selection(selected_index + direction).cloned();

                if let Some(selected_row) = selected_row {
                    if let Some(next_row) = next_row {
                        current_list.select_after(&next_row.id, 300);
                        if let Some(height) = current_list.get_allocated_row_height(&selected_row.id) {
                            current_list.animate_scroll_diff(f64::from(direction * height));
                        }
                    }
                }
            } else {
                let first_row = self.list_model.first().cloned();

                if let Some(first_row) = first_row {
                    current_list.select_after(&first_row.id, 300);
                    current_list.animate_scroll_absolute(0.0);
                }
            }
        }
    }

    pub fn get_selected_article_model(&self) -> Option<ArticleListArticleModel> {
        if let Some(current_list) = self.get_current_list() {
            let selected_index = current_list.get_selected_index();
            if let Some(selected_index) = selected_index {
                return self.list_model.calculate_selection(selected_index).cloned();
            }
        }

        None
    }

    pub fn index_selected(&self, index: i32) {
        let selected_article = self.list_model.calculate_selection(index).cloned();
        if let Some(selected_article) = selected_article {
            if selected_article.read == Read::Unread && !self.global_state.read().get_offline() {
                let update = ReadUpdate {
                    article_id: selected_article.id.clone(),
                    read: Read::Read,
                };
                Util::send(&self.sender, Action::MarkArticleRead(update));
            }

            Util::send(&self.sender, Action::ShowArticle(selected_article.id));
        }
    }

    pub fn set_article_row_state(&mut self, article_id: &ArticleID, read: Option<Read>, marked: Option<Marked>) {
        if let Some(current_list) = self.get_current_list() {
            current_list.set_article_row_state(article_id, read, marked);
            if let Some(read) = read {
                self.list_model.set_read(article_id, read);
            }
            if let Some(marked) = marked {
                self.list_model.set_marked(article_id, marked);
            }
        }
    }
}
